<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
require_once 'simple/simple_html_dom.php';
require_once 'vendor/autoload.php';
require_once 'mysql/vendor/autoload.php';

class html {
	public $token; 
	public $url;
	function __construct(){
		$this->token = "token";
		$this->url = 'https://www.avito.ru';
	}
	function parse($param){
		$search = urlencode($param['search']);
		if($param['category'] != 0){
			$url = "{$this->url}/moskva?pmax={$param['price']}?q={$search}&s=104&user=1";
		}else{
			$url = "{$this->url}/moskva/{$param['category']}?q={$search}&s=104&user=1";
		}
		$html = file_get_html($url);
		$array = $this->htmlToArray($html->find('div[data-marker=item]'));
		$messages = [];
		foreach($array as $item){
			//echo $item->date.' '.$this->checkTime($item->date).'<br>';
			if($this->checkTime($item->date) and $item->price <= $param['price']){
				$messages[] = "Новое объявление ({$item->name}) Цена: {$item->price} Ссылка: {$this->url}{$item->url}";
			}
		}
		return $messages;
	}
	function htmlToArray($html){
		foreach($html as $item){
			$object = [
				'name' => $item->find('h3[itemprop=name]')[0]->innertext,
				'price' => $item->find('span[data-marker=item-price]')[0]->children[1]->content,
				'date' => $item->find('div[data-marker=item-date]')[0]->innertext,
				'url' => $item->find('a[data-marker=item-title]')[0]->href,
			];
			$array[] = (object)$object;
		}
		if($array){
			return $array;
		}else{
			return false;
		}
	}
	function checkTime($time){
		if($time == 'Несколько секунд назад'){
			return true;
		}else{
			$date = explode(' ', $time);
			if($date[0] <= 6){
				switch($date[1]){
					case 'минуту':
						return true;
						break;
					case 'минуты':
						return true;
						break;
					case 'минут':
						return true;
						break;
					default:
						return false;
				}
			}else{
				return false;
			}
		}
	}
	function bot($message){
		$bot = new \TelegramBot\Api\BotApi($this->token);
		$bot->sendMessage(457117636, $message);
		$bot->sendMessage(933057600, $message);
	}

}
class DB{
	public $ip;
	public $login;
	public $pass;
	public $db;
	function __construct(){
		$this->ip = 'ip';
		$this->login = 'user';
		$this->pass = 'pass';
		$this->db = 'avito';
		
	}
	function mysql($sql){
		$mysqli = new mysqli($this->ip, $this->login, $this->pass, $this->db);
		$mysqli->set_charset("utf8");
		if ($mysqli->connect_errno) {
			echo "Извините, возникла проблема при подключении";
			die;
		}
		if (!$result = $mysqli->query($sql)) {
			echo "Извините, возникла проблема при обработке запроса.<br>";
			echo "Ошибка: Наш запрос не удался и вот почему: <br>";
			echo "Запрос: " . $sql . "<br>";
			echo "Номер ошибки: " . $mysqli->errno . "<br>";
			echo "Ошибка: " . $mysqli->error . "<br>";
			die;
		}else{
			return $this->mysqlToArray($result);
		}
	}
	function mysqlToArray($result){
		if($result->num_rows !== 0){
			while ($actor = $result->fetch_assoc()) {
				$array[] = (object)$actor;
			}
			return $array;
		}else{
			return false;
		}
	}
}